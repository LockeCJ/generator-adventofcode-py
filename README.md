# generator-adventofcode-py [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Yeoman Generator for Advent of Code in Python.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-adventofcode-py using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-adventofcode-py
```

Then generate your new project:

```bash
yo adventofcode-py
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

unlicense © [LockeCJ]()


[npm-image]: https://badge.fury.io/js/generator-adventofcode-py.svg
[npm-url]: https://npmjs.org/package/generator-adventofcode-py
[travis-image]: https://travis-ci.com/LockeCJ/generator-adventofcode-py.svg?branch=master
[travis-url]: https://travis-ci.com/LockeCJ/generator-adventofcode-py
[daviddm-image]: https://david-dm.org/LockeCJ/generator-adventofcode-py.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/LockeCJ/generator-adventofcode-py
[coveralls-image]: https://coveralls.io/repos/LockeCJ/generator-adventofcode-py/badge.svg
[coveralls-url]: https://coveralls.io/r/LockeCJ/generator-adventofcode-py
